# Todo


# Schedule

# クラス設計
## StationParam
```
struct StationParam
{
	s3d::String id;
	s3d::String name;
	s3d::String kanaName;
	s3d::String romaName;

	s3d::Color color;
};
```

## StationLine
```
class StationLine
{
public:
	StationLine();
	bool isFill();

private:
	std::vector<StationCircle> m_stationCircles;
};
```

## StationCircle
