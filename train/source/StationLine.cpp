#include "StationLine.h"
#include "StationCircle.h"
#include "StationHolder.h"
#include "StationDataBase.h"
#include "StationParameter.h"
#include "Shifter.h"
#include "extended_std.h"
#include "Camera.h"
#include "Finder.h"
#include "Score.h"

StationLine::StationLine(const StationDataBase& sdb, const s3d::String& lineName, std::shared_ptr<Finder> finder, std::shared_ptr<Shifter> shifter, std::shared_ptr<Camera> camera) :
	m_finder{ finder },
	m_shifter{ shifter },
	m_camera(camera)
{
	const auto& lineParam = sdb.getStationLineParameter(lineName);
	const auto& list = sdb.getParameterList(lineName);

	m_lineColor = lineParam.color;
	m_lineName = lineParam.name;

	for (const auto& s : list) {
		m_stationCircles.emplace_back(std::make_shared<StationCircle>(s, m_camera));
	}

	for (const auto& param : list) {
		const auto& lineNum = param.inLineParameters.size();

		m_stationLinks.push_back(param.pos);
		m_fixedCircles.emplace_back(s3d::Circle{ param.pos - m_camera->pos, 20 }, param);
	}

	// for (const auto& c : m_stationCircles) {
	// 	const auto& param = c->getParameter();
	// 	const auto& lineNum = param.inLineParameters.size();

	// 	m_stationLinks.push_back(c->getPos());
	// 	m_fixedCircles.emplace_back(s3d::Circle{ c->getPos(), 20 }, c->getParameter());
	// 	// m_fixedCircles.emplace_back(s3d::Circle{ c->getPos(), 10.0 * (lineNum + 1)}, c->getParameter());
	// }
}

void StationLine::update()
{
	for (auto&& c : m_fixedCircles) {
		const auto& param{ c.second };
		// if (param.inLineParameters.at(0).lineName != m_lineName) { continue; }

		c.first.setPos(param.pos - m_camera->pos);

		if(c.first.leftClicked) {
			m_shifter->checkMovableToContainer(c.second);
		}
	}

	estd::remove_erase_if(m_stationCircles, [](const std::shared_ptr<StationCircle>& c) { return c->inHolder(); });
	
	++m_timer;
}

void StationLine::draw() const
{
	m_stationLinks.movedBy(-m_camera->pos).draw(8.0, m_lineColor);

	for (const auto& c : m_fixedCircles) {
		const auto& param{ c.second };
		// if (param.inLineParameters.at(0).lineName != m_lineName) { continue; }

		c.first.draw(s3d::Palette::White);
		c.first.drawFrame(0, 2.0, {L"#ddd"});

		if (m_shifter->isContainsInHolder(c.second)) {
			c.first.scaled(0.5 + s3d::Math::Abs(s3d::Math::Sin(m_timer / 30.0))).drawFrame(0, 2.0, s3d::Palette::Red);
		}
	}
}
