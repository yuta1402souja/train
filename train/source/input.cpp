#include "input.h"

namespace input
{
	namespace
	{
		const std::unordered_map<Type, s3d::KeyCombination> KeyCombinations{
			{ Type::Up,    s3d::Input::KeyK | s3d::Input::KeyUp    },
			{ Type::Right, s3d::Input::KeyL | s3d::Input::KeyRight },
			{ Type::Down,  s3d::Input::KeyJ | s3d::Input::KeyDown  },
			{ Type::Left,  s3d::Input::KeyH | s3d::Input::KeyLeft  },
		};
	}

	bool isClicked(Type type) { return KeyCombinations.at(type).clicked; }
	bool isReleased(Type type) { return KeyCombinations.at(type).released; }
	bool isPressed(Type type) { return KeyCombinations.at(type).pressed; }
}