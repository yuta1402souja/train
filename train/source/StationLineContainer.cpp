#include "stdafx.h"
#include "StationLineContainer.h"
#include "StationLine.h"
#include "StationDataBase.h"
#include "Camera.h"
#include "Finder.h"

StationLineContainer::StationLineContainer(const StationDataBase& sdb, std::shared_ptr<Finder> finder, std::shared_ptr<Shifter> shifter, std::shared_ptr<Camera> camera) :
	m_finder{ finder },
	m_shifter{ shifter },
	m_camera{ camera }
{
	for (const auto& line : sdb) {
		const auto& lineName = line.first;
		m_stationLines.emplace_back(std::make_shared<StationLine>(sdb, lineName, finder, shifter, camera));
	}
}

StationLineContainer::~StationLineContainer()
{
}

void StationLineContainer::update()
{
	for (auto&& l : m_stationLines) {
		l->update();
	}
}

void StationLineContainer::draw() const
{
	for (const auto& l : m_stationLines) {
		l->draw();
	}
}