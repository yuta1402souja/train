#include "GameTitle.h"
#include "StageStore.h"
#include "Train.h"
#include "StationCircle.h"
#include "StationLine.h"

GameTitle::GameTitle()
{
}

void GameTitle::init()
{
	s3d::SoundAsset(L"GameTitle").setLoop(true);
	s3d::SoundAsset(L"GameTitle").play();
}

void GameTitle::update()
{
	const auto& startFont = s3d::FontAsset(L"Large")(L"はじめる");
	const auto& startFontRegion = startFont.region(m_startFontPos).stretched(10, 10);

	if (startFontRegion.leftClicked) {
		s3d::SoundAsset(L"Decision").playMulti();
		changeScene(L"StageSelection", 2000);
	}

	if (startFontRegion.mouseOver) {
		++m_mouseOverStart;
	}
	else {
		m_mouseOverStart = 0;
	}

	if (m_mouseOverStart == 1) {
		s3d::SoundAsset(L"Select").playMulti();
		m_stretchableLine.setHiddenRegion(startFontRegion);
		m_stretchableLine.restart();
	}

	const auto& endFont = s3d::FontAsset(L"Large")(L"おわる");
	const auto& endFontRegion = endFont.region(m_endFontPos).stretched(10, 10);

	if (endFontRegion.leftClicked) {
		s3d::SoundAsset(L"Decision").playMulti();
		s3d::System::Exit();
	}

	if (endFontRegion.mouseOver) {
		++m_mouseOverEnd;
	}
	else {
		m_mouseOverEnd = 0;
	}

	if (m_mouseOverEnd == 1) {
		s3d::SoundAsset(L"Select").playMulti();
		m_stretchableLine.setHiddenRegion(endFontRegion);
		m_stretchableLine.restart();
	}

	m_stretchableLine.update();
}

void GameTitle::draw() const
{
	s3d::TextureAsset(L"TitleLogo").drawAt(s3d::Window::Center() - s3d::Vec2{ 0, 200 });

	const auto& startFont = s3d::FontAsset(L"Large")(L"はじめる");
	const auto& startFontRegion = startFont.region(m_startFontPos).stretched(10, 10);

	if (startFontRegion.mouseOver) {
		startFontRegion.drawFrame(1.0, 0.0, s3d::Palette::Black);
		startFont.draw(m_startFontPos, s3d::Palette::Black);
	}
	else {
		startFont.draw(m_startFontPos, s3d::Palette::Black);
	}

	const auto& endFont = s3d::FontAsset(L"Large")(L"おわる");
	const auto& endFontRegion = endFont.region(m_endFontPos).stretched(10, 10);

	if (endFontRegion.mouseOver) {
		endFontRegion.drawFrame(1.0, 0.0, s3d::Palette::Black);
		endFont.draw(m_endFontPos, s3d::Palette::Black);
	}
	else {
		endFont.draw(m_endFontPos, s3d::Palette::Black);
	}

	m_stretchableLine.draw();
}