#pragma once

/*!
@brief s3d::TimerSecをこのゲーム用にラップしたクラス
*/
class GameTimer
{
public:

	GameTimer();
	~GameTimer() = default;

	void start();
	void restart();
	void pause();
	void resume();

	void update();
	void draw() const;

	bool isEnd() const { return m_timeLeft <= 0; }

private:

	const s3d::int32 TimeLimit = 120000;
	// const s3d::int32 TimeLimit = 30000;

	s3d::Size m_size;
	s3d::Vec2 m_pos;

	s3d::int32 m_timeLeft{ TimeLimit };
	s3d::Stopwatch m_timer;
	// s3d::TimerMillisec m_timer;

	s3d::RoundRect m_backRect;
};