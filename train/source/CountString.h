#pragma once

class CountString
{
public:
	CountString(std::initializer_list<s3d::String> l);
	~CountString() = default;

	void update();
	void draw() const;

private:
	std::vector<s3d::String> m_strings;
};
