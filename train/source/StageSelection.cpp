#include "StageSelection.h"
#include "input.h"

namespace
{
	const std::unordered_map<StageName, s3d::Rect> ToLogoRect{
		{ StageName::Nagoya, { 0,   0, 326, 101 } },
		{ StageName::Osaka , { 0, 101, 326, 101 } },
		{ StageName::Tokyo , { 0, 202, 326, 101 } },
	};

	const std::unordered_map<StageName, s3d::String> ToString{
		{ StageName::Nagoya, L"Nagoya"},
		{ StageName::Osaka , L"Osaka"},
		{ StageName::Tokyo , L"Tokyo"},
	};

	void decrementStageName(StageName& stageName) {
		int nextStageNameNum = (StageNum + static_cast<int>(stageName) - 1) % StageNum;
		stageName = static_cast<StageName>(nextStageNameNum);
	}

	void incrementStageName(StageName& stageName) {
		int nextStageNameNum = (static_cast<int>(stageName) + 1) % StageNum;
		stageName = static_cast<StageName>(nextStageNameNum);
	}
}

StageSelection::StageSelection()
{
}

void StageSelection::init()
{
	const auto& pos = s3d::Window::Center().movedBy(0, -280);
	const auto& size = s3d::TextureAsset(L"StageNameLogos")(ToLogoRect.at(m_currentStageName)).size;

	m_strechableLine.setHiddenRegion({ pos - size / 2, size });
	m_strechableLine.restart();

	const auto& decisionFont = s3d::FontAsset(L"Large")(L"決定");
	const auto& decisionFontRegion = decisionFont.regionCenter(DecisionFontPos).stretched(40, 20);

	m_decisionStrechableLine.setHiddenRegion(decisionFontRegion);
}

void StageSelection::update()
{
	const auto& decisionFont = s3d::FontAsset(L"Large")(L"決定");
	const auto& decisionFontRegion = decisionFont.regionCenter(DecisionFontPos).stretched(40, 20);

	if (decisionFontRegion.leftClicked) {
		m_data->currentStageName = m_currentStageName;
		s3d::SoundAsset(L"Decision").playMulti();
		s3d::SoundAsset(L"GameTitle").stop();
		changeScene(L"GameMain", 2000);
	}

	if (decisionFontRegion.mouseOver) {
		++m_mouseOverDecision;
	}
	else {
		m_mouseOverDecision = 0;
	}

	if (m_mouseOverDecision == 1) {
		s3d::SoundAsset(L"Select").playMulti();
		m_decisionStrechableLine.restart();
	}

	if (m_leftButton.leftClicked || input::isClicked(input::Type::Left)) {
		s3d::SoundAsset(L"Page").playMulti();
		decrementStageName(m_currentStageName);
		m_strechableLine.restart();
	}

	if (m_rightButton.leftClicked || input::isClicked(input::Type::Right)) {
		s3d::SoundAsset(L"Page").playMulti();
		incrementStageName(m_currentStageName);
		m_strechableLine.restart();
	}

	if (m_helpButton.leftClicked) {
		changeScene(L"GameHelp", 100);
	}

	m_strechableLine.update();
	m_decisionStrechableLine.update();
}

void StageSelection::updateFadeIn(double t)
{
	StageSelection::update();
}

void StageSelection::draw() const
{
	const auto& outlineTexture = s3d::TextureAsset(ToString.at(m_currentStageName) + L"Outline");
	auto& outlineRect = s3d::Rect{ outlineTexture.size };
	outlineRect.setCenter(s3d::Window::Center());

	outlineTexture.drawAt(s3d::Window::Center());
	outlineRect.drawFrame(1.0, 0.0, s3d::Palette::Black);

	// 都市名のロゴを描画
	s3d::TextureAsset(L"StageNameLogos")(ToLogoRect.at(m_currentStageName)).drawAt(s3d::Window::Center().movedBy(0, -280));

	const auto& leftTriangle = s3d::Triangle{ m_leftButton.x, m_leftButton.y, m_leftButton.r }.rotated(-s3d::Math::Pi / 2);
	const auto& rightTriangle = s3d::Triangle{ m_rightButton.x, m_rightButton.y, m_rightButton.r }.rotated(s3d::Math::Pi / 2);

	if (m_leftButton.mouseOver) {
		m_leftButton.scaled(1.2).drawFrame(1.0, 1.0, s3d::Palette::Black);
		leftTriangle.draw(s3d::Palette::Black);
	}
	else {
		m_leftButton.draw(s3d::Palette::Black);
		leftTriangle.draw(s3d::Palette::White);
	}

	if (m_rightButton.mouseOver) {
		m_rightButton.scaled(1.2).drawFrame(1.0, 1.0, s3d::Palette::Black);
		rightTriangle.draw(s3d::Palette::Black);
	}
	else
	{
		m_rightButton.draw(s3d::Palette::Black);
		rightTriangle.draw(s3d::Palette::White);
	}

	if (m_helpButton.mouseOver) {
		m_helpButton.scaled(1.2).drawFrame(1.0, 1.0, s3d::Palette::Black);
		s3d::FontAsset(L"Middle")(L"?").drawCenter(m_helpButton.center, s3d::Palette::Black);
	}
	else
	{
		m_helpButton.draw(s3d::Palette::Black);
		s3d::FontAsset(L"Middle")(L"?").drawCenter(m_helpButton.center, s3d::Palette::White);
	}

	const auto& decisionFont = s3d::FontAsset(L"Large")(L"決定");
	const auto& decisionFontRegion = decisionFont.regionCenter(DecisionFontPos).stretched(40, 20);

	if (decisionFontRegion.mouseOver) {
		decisionFontRegion.draw(s3d::Palette::White);
		decisionFontRegion.drawFrame(1.0, 0.0, s3d::Palette::Black);
		decisionFont.drawCenter(DecisionFontPos, s3d::Palette::Black);
	}
	else {
		decisionFontRegion.draw(s3d::Palette::Black);
		decisionFont.drawCenter(DecisionFontPos, s3d::Palette::White);
	}

	m_strechableLine.draw();
	m_decisionStrechableLine.draw();
}