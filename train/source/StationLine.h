#pragma once

#include "StationParameter.h"

class StationCircle;
class StationHolder;
class StationDataBase;
class Finder;
class Shifter;
struct Camera;

class StationLine
{
public:
	StationLine(const StationDataBase& sdb, const s3d::String& lineName, std::shared_ptr<Finder> finder, std::shared_ptr<Shifter> shifter, std::shared_ptr<Camera> camera);

	void update();
	void draw() const;

	void setShifter(std::shared_ptr<Shifter> shifter) { m_shifter = shifter; }

private:
	std::shared_ptr<Finder> m_finder;
	std::shared_ptr<Shifter> m_shifter;
	std::shared_ptr<Camera> m_camera;

	s3d::Color m_lineColor;
	s3d::String m_lineName;

	std::vector<std::shared_ptr<StationCircle>> m_stationCircles;
	s3d::LineString m_stationLinks;
	std::vector<std::pair<s3d::Circle, StationParameter>> m_fixedCircles;

	int m_timer{0};
};