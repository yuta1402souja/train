#pragma once
#include "GameDefine.h"

class CameraWork;
class Shifter;
class Finder;
class StationHolder;
class StationCircleContainer;
class StationLineContainer;
class TrainContainer;
class GameTimer;
struct Camera;

class GameMain : public Scene
{
public:
	GameMain();
	~GameMain() = default;

	void init() override;
	void update() override;
	void GameMain::updateFadeIn(double t);
	void draw() const override;

private:
	std::shared_ptr<Camera> m_camera;
	std::unique_ptr<CameraWork> m_cameraWork;

	std::shared_ptr<StationHolder> m_stationHolder;
	std::shared_ptr<StationLineContainer> m_stationLineContainer;
	std::shared_ptr<StationCircleContainer> m_stationCircleContainer;
	std::shared_ptr<TrainContainer> m_trainContainer;

	std::shared_ptr<Shifter> m_shifter;
	std::shared_ptr<Finder> m_finder;

	std::unique_ptr<GameTimer> m_gameTimer;
};
