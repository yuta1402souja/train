#include "stdafx.h"
#include "Camera.h"
#include "input.h"

CameraWork::CameraWork()
{
}

CameraWork::CameraWork(std::shared_ptr<Camera> camera) :
	m_camera(camera)
{
}

CameraWork::CameraWork(const s3d::Vec2 & pos)
{
}

void CameraWork::update()
{
	if (!m_camera) { return; }

	m_camera->pos.x -= 6 * static_cast<int>(input::isPressed(input::Type::Left));
	m_camera->pos.x += 6 * static_cast<int>(input::isPressed(input::Type::Right));
	m_camera->pos.y -= 6 * static_cast<int>(input::isPressed(input::Type::Up));
	m_camera->pos.y += 6 * static_cast<int>(input::isPressed(input::Type::Down));

	if (s3d::Input::MouseR.pressed) {
		m_camera->pos -= s3d::Mouse::Delta();
	}

	if (!s3d::Window::GetState().focused) {
		return;
	}

	const auto& mousePos = s3d::Mouse::Pos();

	if (RightRect.intersects(mousePos)) { m_camera->pos.x += 6; }
	if (LeftRect.intersects(mousePos)) { m_camera->pos.x -= 6; }
	if (TopRect.intersects(mousePos)) { m_camera->pos.y -= 6; }
	if (BottomRect.intersects(mousePos)) { m_camera->pos.y += 6; }

	m_camera->pos.x = s3d::Clamp(m_camera->pos.x, m_camera->region.x, m_camera->region.w - s3d::Window::Width());
	m_camera->pos.y = s3d::Clamp(m_camera->pos.y, m_camera->region.y, m_camera->region.h - s3d::Window::Height());
}

void CameraWork::draw() const
{
	const auto& mousePos = s3d::Mouse::Pos();

	if (RightRect.intersects(mousePos)) { s3d::TextureAsset(L"IndicatorScrollRight").draw(); }
	if (LeftRect.intersects(mousePos)) { s3d::TextureAsset(L"IndicatorScrollLeft").draw(); }
	if (TopRect.intersects(mousePos)) { s3d::TextureAsset(L"IndicatorScrollTop").draw(); }
	if (BottomRect.intersects(mousePos)) { s3d::TextureAsset(L"IndicatorScrollBottom").draw(); }
}