#include "GameHelp.h"

GameHelp::GameHelp()
{
	s3d::TextReader textReader{L"./data/description.txt"};

	s3d::String line;
	while (textReader.readLine(line)) {
		const s3d::String title{ line };

		s3d::String description{ L"" };
		while (1) {
			textReader.readLine(line);
			if (line == L"END") { break; }
			description += line;
			description += L"\n";
		}

		m_descriptions.emplace_back(title, description);
	}
}

void GameHelp::init()
{
}

void GameHelp::update()
{
	if (s3d::Input::MouseL.clicked) {
		++m_current;
	}

	if (m_current >= m_descriptions.size()) {
		m_current = m_descriptions.size() - 1;
		changeScene(L"StageSelection", 100);
	}
}

void GameHelp::draw() const
{
	s3d::TextureAsset(L"NavigationCharacter0").draw(s3d::Vec2{ 400, 0 });
	s3d::Rect{ 50, 50, 600, 668 }.draw(s3d::Color{ s3d::Palette::Black, 200 });
	s3d::FontAsset(L"HelpTitle")(m_descriptions.at(m_current).first).drawCenter({ 340, 150 }, s3d::Palette::White);
	s3d::FontAsset(L"HelpDescription")(m_descriptions.at(m_current).second).draw({ 75, 230 }, s3d::Palette::White);
}