#include "stdafx.h"
#include "Shifter.h"
#include "StationHolder.h"
#include "StationLine.h"
#include "StationCircle.h"
#include "StationCircleContainer.h"
#include "Score.h"

Shifter::Shifter()
{
}

void Shifter::shiftToStationHolder(std::shared_ptr<StationCircle> stationCircle)
{
	m_stationHolder->addStationCircle(stationCircle);
	Score::getInstance()->addScore(50);
}

void Shifter::shiftToStationCircleContainer(std::shared_ptr<StationCircle> stationCircle)
{
	m_stationCircleContainer->addStationCircle(stationCircle);
	Score::getInstance()->addScore(200);
}

void Shifter::checkMovableToContainer(const StationParameter & param)
{
	m_stationHolder->checkMovableToLine(param);
}

bool Shifter::isContainsInHolder(const StationParameter& param) const
{
	return m_stationHolder->contains(param);
}