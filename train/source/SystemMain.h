#pragma once

#include "GameDefine.h"

class SystemMain
{
public:
	SystemMain();
	~SystemMain() = default;

	void MainLoop();

private:

	ham::SceneManager<s3d::String, GameData> m_sceneManager;
};
