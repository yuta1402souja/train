#pragma once

#include <Siv3D.hpp>

class StretchableLine
{
public:
	StretchableLine();
	StretchableLine(const s3d::Rect& hiddenRegion);
	~StretchableLine() = default;

	void restart();

	void update();
	void draw() const;

	void setHiddenRegion(const s3d::Rect& hiddenRegion);
	void setHiddenRegion(const s3d::RectF& hiddenRegion);

	bool isPaused() const { return m_stopwatch.isPaused(); }

private:
	s3d::int32 m_stretchingTime{ 500 };

	s3d::Vec2 m_pos;
	s3d::Vec2 m_leftStartPos;
	s3d::Vec2 m_rightStartPos;

	s3d::Line m_leftLine;
	s3d::Line m_rightLine;

	s3d::Stopwatch m_stopwatch;
};