#pragma once

#include "Singleton.h"
#include "GameDefine.h"

#include "StationDataBase.h"
// class StationDataBase;

class StageStore : public Singleton<StageStore>
{
private:
	friend class Singleton<StageStore>;
	using Stage = StationDataBase;

public:
	StageStore();
	~StageStore() = default;

	const Stage& getStage(const StageName& stageName) { return m_stages.at(stageName); }

private:
	const s3d::String stageFileDirectory{ L"./data/stage/"  };
	const s3d::String stageListFileName { L"stage_list.txt" };
	const s3d::String LineListFileName  { L"LineList.txt"   };

	std::unordered_map<StageName, Stage> m_stages;
	std::unordered_map<StageName, s3d::Size> m_stageSizes;
};
