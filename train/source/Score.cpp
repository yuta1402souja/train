#include "Score.h"

Score::Score()
{
}

Score::~Score()
{
}

void Score::init()
{
	m_score = 0;
}

void Score::update()
{
}

void Score::draw() const
{
	// m_rect.draw(s3d::Palette::White);
	// m_rect.drawFrame(0, 2.0, s3d::Palette::Black);

	s3d::FontAsset(L"Middle")(L"Score: ").drawCenter(m_rect.pos - m_rect.size / 2, s3d::Palette::White);
	s3d::FontAsset(L"Middle")(m_score).drawCenter(m_rect.pos + m_rect.size / 2, s3d::Palette::White);

	m_effect.update();
}

void Score::addScore(int value)
{
	m_score += value;

	if (value > 0) {
		m_effect.add<IncreaseScore>(s3d::FontAsset(L"Small"), s3d::ToString(value), m_rect.pos + s3d::Vec2{ 20 , m_rect.size.y });
	}
	else {
		m_effect.add<DecreaseScore>(s3d::FontAsset(L"Small"), s3d::ToString(value), m_rect.pos + s3d::Vec2{ 20 , m_rect.size.y });
	}
}