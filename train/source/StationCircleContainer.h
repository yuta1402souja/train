#pragma once

class Shifter;
class StationCircle;
struct StationParameter;
struct Camera;

class StationCircleContainer
{
public:
	StationCircleContainer(std::shared_ptr<Shifter> shifter, std::shared_ptr<Camera> camera);
	~StationCircleContainer();

	void addStationCircle(std::shared_ptr<StationCircle> stationCircle);

	void update();
	void draw() const;

	bool findInLine(const StationParameter& stationParameter) const;

private:
	std::shared_ptr<Shifter> m_shifter;
	std::shared_ptr<Camera> m_camera;
	std::vector<std::shared_ptr<StationCircle>> m_stationCircles;

	int m_circleInMapCount;

	s3d::Stopwatch m_circleThrottleTimer;
};
