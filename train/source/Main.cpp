#include "SystemMain.h"

void Main()
{
	auto systemMain = std::make_unique<SystemMain>();
	systemMain->MainLoop();
}