#include "SystemMain.h"
#include "GameTitle.h"
#include "GameMain.h"
#include "StageSelection.h"
#include "GameResult.h"
#include "GameHelp.h"

SystemMain::SystemMain()
{
	s3d::Window::Resize(1024, 768);
	s3d::Window::SetTitle(L"Train");
	s3d::Graphics::SetBackground({L"#EEE"});

	std::mt19937 engine(static_cast<int>(time(nullptr)));
	srand(engine());

	s3d::FontAsset::Register(L"Small", 10, L"�l�r ����");
	s3d::FontAsset::Register(L"Middle", 14, L"�l�r ����");
	s3d::FontAsset::Register(L"Large", 20, L"�l�r ����");

	s3d::FontAsset::Register(L"Timer", 40);

	s3d::FontAsset::Register(L"ResultScore", 64, L"�l�r ����");

	s3d::FontAsset::Register(L"HelpTitle", 32, L"�l�r ����");
	s3d::FontAsset::Register(L"HelpDescription", 12, L"�l�r ����");

	s3d::TextureAsset::Register(L"NagoyaSideTrains", L"./data/image/nagoya_side_trains.png");
	s3d::TextureAsset::Register(L"TokyoSideTrains", L"./data/image/tokyo_side_trains.png");
	s3d::TextureAsset::Register(L"OsakaSideTrains", L"./data/image/osaka_side_trains.png");
	s3d::TextureAsset::Register(L"Background", L"./data/image/background.png");
	s3d::TextureAsset::Register(L"TitleLogo", L"./data/image/title_logo.png");

	const s3d::String ImageDirectory = L"./data/image/";

	s3d::TextureAsset::Register(L"NagoyaTrains", ImageDirectory + L"nagoya_trains.png");
	s3d::TextureAsset::Register(L"TokyoTrains",  ImageDirectory + L"tokyo_trains.png");
	s3d::TextureAsset::Register(L"OsakaTrains",  ImageDirectory + L"osaka_trains.png");

	s3d::TextureAsset::Register(L"NagoyaOutline", ImageDirectory + L"nagoya_outline.png");
	s3d::TextureAsset::Register(L"TokyoOutline", ImageDirectory + L"tokyo_outline.png");
	s3d::TextureAsset::Register(L"OsakaOutline", ImageDirectory + L"osaka_outline.png");

	s3d::TextureAsset::Register(L"StageNameLogos", ImageDirectory + L"stage_name_logos.png");

	s3d::TextureAsset::Register(L"IndicatorFrame", ImageDirectory + L"indicator/frame.png");
	s3d::TextureAsset::Register(L"IndicatorScrollLeft", ImageDirectory + L"indicator/scroll_left.png");
	s3d::TextureAsset::Register(L"IndicatorScrollRight", ImageDirectory + L"indicator/scroll_right.png");
	s3d::TextureAsset::Register(L"IndicatorScrollTop", ImageDirectory + L"indicator/scroll_top.png");
	s3d::TextureAsset::Register(L"IndicatorScrollBottom", ImageDirectory + L"indicator/scroll_bottom.png");

	s3d::TextureAsset::Register(L"StageNameLogos", ImageDirectory + L"stage_name_logos.png");

	s3d::TextureAsset::Register(L"ResultFrame", ImageDirectory + L"result_frame.png");

	s3d::TextureAsset::Register(L"NavigationCharacter0", ImageDirectory + L"navigation_character0.png");
	s3d::TextureAsset::Register(L"NavigationCharacter1", ImageDirectory + L"navigation_character1.png");


	const s3d::String SoundDirectory = L"./data/sound/";
	const s3d::String BGMDirectory = SoundDirectory + L"bgm/";
	const s3d::String SEDirectory = SoundDirectory + L"se/";

	const std::unordered_map<s3d::String, s3d::String> BGMFileNames{
		{ L"GameMain" , L"game_main"  },
		{ L"GameTitle", L"game_title" }
	};

	for (const auto& pair : BGMFileNames) {
		s3d::SoundAsset::Register(pair.first, BGMDirectory + pair.second + L".wav");
	}

	for (int i = 1; i <= 17; ++i) {
		s3d::SoundAsset::Register(L"Result" + s3d::ToString(i), BGMDirectory + L"result/" + s3d::ToString(i) + L".wav");
	}

	const std::unordered_map<s3d::String, s3d::String> SEFileNames{
		{ L"Cancel"       , L"cancel"  },
		{ L"Decision"     , L"decision" },
		{ L"Finish"       , L"finish" },
		{ L"OutStation"   , L"out_station" },
		{ L"Page"         , L"page"},
		{ L"PickUpStation", L"pick_up_station" },
		{ L"PutStation"   , L"put_station"     },
		{ L"Select"       , L"select"          },
	};

	for (const auto& pair : SEFileNames) {
		s3d::SoundAsset::Register(pair.first, SEDirectory + pair.second + L".wav");
	}
}

void SystemMain::MainLoop()
{
	// �t�F�[�h�C���E�A�E�g���̐F
	m_sceneManager.setFadeColor(s3d::Palette::White);

	// �V�[����ݒ�
	m_sceneManager.add<GameTitle>(L"GameTitle");
	m_sceneManager.add<StageSelection>(L"StageSelection");
	m_sceneManager.add<GameMain>(L"GameMain");
	m_sceneManager.add<GameResult>(L"GameResult");
	m_sceneManager.add<GameHelp>(L"GameHelp");

	m_sceneManager.init(L"GameTitle");
	// m_sceneManager.init(L"GameMain");
	// m_sceneManager.init(L"StageSelection");

	int y{ 0 };

	while (s3d::System::Update())
	{
		if (y / 3 >= s3d::Window::Height()) {
			y = 0;
		}

		s3d::TextureAsset(L"Background").draw(0, y / 3);
		s3d::TextureAsset(L"Background").draw(0, y / 3 - s3d::Window::Height());

		if (!m_sceneManager.updateAndDraw()) {
			break;
		}
		
		// s3d::FontAsset(L"Small")(s3d::Mouse::Pos()).draw(s3d::Mouse::Pos(), s3d::Palette::Black);

		++y;
	}
}