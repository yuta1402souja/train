#include "StationCircleContainer.h"
#include "Shifter.h"
#include "StationCircle.h"
#include "Camera.h"
#include "extended_std.h"
#include "StationParameter.h"

StationCircleContainer::StationCircleContainer(std::shared_ptr<Shifter> shifter, std::shared_ptr<Camera> camera) :
	m_shifter(shifter),
	m_camera(camera),
	m_circleThrottleTimer{ true },
	m_circleInMapCount{ 0 }
{
}

StationCircleContainer::~StationCircleContainer()
{
}

void StationCircleContainer::update()
{
	for (auto&& c : m_stationCircles) {
		c->update(m_camera);

		if (c->canMoveToStationHolder()) {
			s3d::SoundAsset(L"PickUpStation").playMulti();
			m_shifter->shiftToStationHolder(c);
		}
	}

	// if (s3d::Input::KeyR.clicked) {
	// 	std::random_shuffle(m_stationCircles.begin(), m_stationCircles.end());
	// 	for (int i{ 0 }; i < std::min(static_cast<size_t>(3), m_stationCircles.size()) ; ++i) {
	// 		auto&& c = m_stationCircles[i];
	// 		c->moveRandomPos(m_camera);
	// 	}
	// }

	// if (s3d::Input::KeyA.clicked) {
	// 	for (auto&& c : m_stationCircles) {
	// 		c->moveRandomPos(m_camera);
	// 	}
	// }

	if (m_circleThrottleTimer.ms() > 5000) {
		std::random_shuffle(m_stationCircles.begin(), m_stationCircles.end());

		s3d::SoundAsset(L"OutStation").playMulti();

		for (int i{ 0 }; i < std::min(static_cast<size_t>(1), m_stationCircles.size()) ; ++i) {
			// if (m_circleInMapCount > 10) { break; }

			auto&& c = m_stationCircles[i];
			c->moveRandomPos(m_camera);
			// ++m_circleInMapCount;
		}

		m_circleThrottleTimer.restart();
	}

	estd::remove_erase_if(m_stationCircles, [](const std::shared_ptr<StationCircle>& c) { return c->inHolder(); });
}

void StationCircleContainer::draw() const
{
	for (const auto& c : m_stationCircles) {
		c->draw();
	}
}

bool StationCircleContainer::findInLine(const StationParameter& stationParameter) const
{
	auto it = std::find_if(
		m_stationCircles.begin(),
		m_stationCircles.end(),
		[stationParameter](const std::shared_ptr<StationCircle>& c) { return (*c) == stationParameter && c->inLine(); }
	);

	if (it == m_stationCircles.end()) {
		return false;
	}

	return true;
}

void StationCircleContainer::addStationCircle(std::shared_ptr<StationCircle> stationCircle)
{
	auto it{ std::find_if(m_stationCircles.begin(), m_stationCircles.end(),[stationCircle](const std::shared_ptr<StationCircle>& c) {
		return (*c) == (*stationCircle);
	}) };

	// バグを持ってバグを制す
	// パラメータのinLineParametersは大きいやつも小さいやつもある
	// なので一番大きいものをとる
	if (it != m_stationCircles.end()) {
		if (stationCircle->getParameter().inLineParameters.size() > (*it)->getParameter().inLineParameters.size())
		{
			(*it) = stationCircle;
			return;
		}
	}

	// すでに存在していたら追加しない
	if (it != m_stationCircles.end()) { return; }

	m_stationCircles.emplace_back(stationCircle);
	stationCircle->moveToStationLine(m_camera);
}