#pragma once

#include "Singleton.h"

struct DecreaseScore : public s3d::IEffect
{
	const s3d::Font m_font;
	const s3d::String m_str;
	const s3d::Vec2 m_pos;

	DecreaseScore(const s3d::Font& font, const s3d::String& str, const s3d::Vec2& pos) :
		m_font(font),
		m_str(str),
		m_pos(pos)
	{
	}

	bool update(double t) override
	{
		if (t >= 1.0) { return false; }

		const s3d::uint32 alpha = 255 * (1.0 - t);

		m_font(m_str).drawCenter(m_pos + s3d::Vec2(0, -40 + 40 * t), s3d::Color(s3d::Palette::Red, alpha));

		return true;
	}
};

struct IncreaseScore : public s3d::IEffect
{
	const s3d::Font m_font;
	const s3d::String m_str;
	const s3d::Vec2 m_pos;

	IncreaseScore(const s3d::Font& font, const s3d::String& str, const s3d::Vec2& pos) :
		m_font(font),
		m_str(str),
		m_pos(pos)
	{
	}

	bool update(double t) override
	{
		if (t >= 1.0) { return false; }

		const s3d::uint32 alpha = 255 * (1.0 - t);

		m_font(m_str).drawCenter(m_pos + s3d::Vec2(0, -40 * t), s3d::Color(s3d::Palette::Orange, alpha));

		return true;
	}
};

class Score : public Singleton<Score>
{
private:
	friend class Singleton<Score>;

public:
	Score();
	~Score();

	void init();
	void update();
	void draw() const;

	void addScore(int value);

	int getScore() const { return m_score; }

private:
	int m_score{ 0 };

	s3d::Effect m_effect;
	s3d::Rect m_rect{ { s3d::Window::Width() - 250, 0 }, { 100, 60 } };
};