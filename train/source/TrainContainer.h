#pragma once

struct Camera;
class Finder;
// class Train;
class StationDataBase;

#include "Train.h"

class TrainContainer
{
public:
	TrainContainer(const StationDataBase& sdb, std::shared_ptr<Finder> finder, std::shared_ptr<Camera> camera);
	~TrainContainer() = default;

	void update();
	void draw() const;

private:
	std::shared_ptr<Camera> m_camera;
	std::vector<Train> m_trains;
};