#pragma once

struct Camera;
struct StationParameter;
class StationDataBase;
class Finder;

#include "ReversibleCounter.h"

/*!
@brief Train on station line
*/
class Train
{
public:
	Train(const StationDataBase& sdb, const s3d::String& lineName, std::shared_ptr<Finder> finder);
	~Train() = default;

	void update(std::shared_ptr<Camera> camera);
	void draw() const;

private:
	const double Speed{ 30 };

	const s3d::String m_lineName;
	const s3d::Color m_lineColor;

	std::shared_ptr<Finder> m_finder;
	std::vector<StationParameter> m_stationLinks;

	bool isReverse{ false };

	int m_timer{ 0 };
	ReversibleCounter m_currentLink;

	int m_nextTime{ 60 };

	s3d::Vec2 m_pos{ 0, 0 };

	int m_nextEmptyCircleCount{ 0 };
};