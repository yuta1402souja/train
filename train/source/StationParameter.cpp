#include "StationParameter.h"

StationParameter::StationParameter(const s3d::String & _name, const s3d::String & _kanaName, const s3d::Vec2 & _pos, int _drawNameDirection) :
	name{ _name },
	kanaName{ _kanaName },
	pos{ _pos },
	drawNameDirection{ _drawNameDirection }
{}

void StationParameter::addInLineParameter(const InLineParameter& inLineParameter)
{
	inLineParameters.emplace_back(inLineParameter);
}

/*!
@note id, nameが一致すれば駅は一致する
*/
bool StationParameter::operator == (const StationParameter& rhs) const
{
	// if (id == rhs.id && name == rhs.name) { return true; }
	if (name == rhs.name) { return true; }

	return false;
}