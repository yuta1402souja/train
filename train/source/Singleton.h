#pragma once

template<class _T>
class Singleton
{
protected:

	Singleton() = default;
	virtual ~Singleton() = default;

public:

	Singleton(const Singleton& rhs) = delete;
	Singleton& operator=(const Singleton& rhs) = delete;
	Singleton(Singleton&&) = delete;
	Singleton& operator=(Singleton&&) = delete;
	
	static _T* getInstance()
	{
		static _T instance;
		return &instance;
	}
};