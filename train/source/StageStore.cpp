#include "StageStore.h"
#include "StationDataBase.h"

namespace
{
	const std::unordered_map<s3d::String, StageName> ToStageName = {
		{ L"nagoya", StageName::Nagoya},
		{ L"tokyo" , StageName::Tokyo},
		{ L"osaka" , StageName::Osaka},
	};

	using Stage = StationDataBase;
}

StageStore::StageStore()
{
	s3d::TextReader stageListReader{ stageFileDirectory + stageListFileName };
	s3d::String stageNameString;

	while (stageListReader.readLine(stageNameString)) {
		const s3d::String LineListFilePath{ stageFileDirectory + stageNameString + L"/" + LineListFileName };
		const Stage stage{ stageFileDirectory + stageNameString + L"/"};

		m_stages.emplace(ToStageName.at(stageNameString), std::move(stage));
	}
}