#include "stdafx.h"
#include "TrainContainer.h"
#include "Camera.h"
#include "StationLine.h"
#include "Train.h"
#include "StationDataBase.h"

TrainContainer::TrainContainer(const StationDataBase& sdb, std::shared_ptr<Finder> finder, std::shared_ptr<Camera> camera) :
	m_camera{ camera }
{
	for (const auto& line : sdb) {
		const auto& lineName = line.first;
		m_trains.emplace_back(sdb, lineName, finder);
	}
}

void TrainContainer::update()
{
	for (auto&& t : m_trains) {
		t.update(m_camera);
	}
}

void TrainContainer::draw() const
{
	for (const auto& t : m_trains) {
		t.draw();
	}
}