#include "StationDataBase.h"
#include "extended_std.h"

using ParameterList = std::vector<StationParameter>;

StationDataBase::StationDataBase() :
	StationDataBase(L"./LineList.txt")
{
}

StationDataBase::StationDataBase(const s3d::String & fileDirectory)
{
	s3d::TextReader lineListReader{ fileDirectory + L"LineList.txt"};
	s3d::String shortLineName;

	// 路線ごとに読み込みを行う
	while (lineListReader.readLine(shortLineName)) {
		// mapに路線名とリストを登録
		// m_stationParameterMap.insert(std::make_pair(shortLineName, ParameterList{}));
		m_stationParameterMap.emplace(shortLineName, ParameterList{});

		s3d::CSVReader csv(fileDirectory + shortLineName + L".csv");

		const int StationNum{ csv.get<int>(0, 0) };

		const s3d::String lineName{ csv.get<s3d::String>(0,1) };
		const s3d::String lineKanaName{ csv.get<s3d::String>(0,2) };

		const int lx{ csv.get<int>(0,3) };
		const int ly{ csv.get<int>(0,4) };
		const s3d::Vec2 linePos{ lx, ly };
		const s3d::Color lineColor{ csv.get<s3d::String>(0, 5) };

		StationLineParameter lineParam{ lineName, lineKanaName, linePos, lineColor };
		m_stationLineMap.emplace(shortLineName, std::move(lineParam));

		auto& list{ m_stationParameterMap.at(shortLineName) };

		for (int col{ 1 }; col <= StationNum; ++col) {
			const s3d::String id{ csv.get<s3d::String>(col, 0) };
			const s3d::String name{ csv.get<s3d::String>(col, 1) };
			const s3d::String kanaName{ csv.get<s3d::String>(col, 2) };

			const int x{ csv.get<int>(col, 3) };
			const int y{ csv.get<int>(col, 4) };
			const s3d::Vec2 localPos{ x, y };

			const int drawNameDirection{ csv.get<int>(col, 5) };

			list.emplace_back(name, kanaName, linePos + localPos, drawNameDirection);

			// 現在の駅がm_stationParameterMapに存在するかをチェックする
			// 存在するなら各駅の路線情報を更新する
			for (auto&& m : m_stationParameterMap) {
				auto l = m.second;
				auto it = std::find_if(l.begin(), l.end(), [name](const StationParameter& sp) { return name == sp.name; });

				if (it != l.end()) {
					// it->addInLineParameter({ lineName, id, lineColor, linePos, localPos });
					it->inLineParameters.emplace_back(lineName, id, lineColor, linePos, localPos);
					list.back().inLineParameters = it->inLineParameters;
				}
			}
		}
	}

	// なぜか重複してしまっている各駅の路線情報を1つにする
	// もっと頭の良いやり方あるはず
	// 要修正
	for (auto&& list : m_stationParameterMap) {
		for (auto&& c : list.second) {
			estd::sort(c.inLineParameters);
			c.inLineParameters.erase(std::unique(c.inLineParameters.begin(), c.inLineParameters.end()), c.inLineParameters.end());
		}
	}

	s3d::Size stageSize{ 0, 0 };
	for (auto&& list : m_stationParameterMap) {
		for (auto&& c : list.second) {
			stageSize.x = std::max(stageSize.x, static_cast<s3d::int32>(c.pos.x));
			stageSize.y = std::max(stageSize.y, static_cast<s3d::int32>(c.pos.y));
		}
	}

	m_stageRegion.set(-StagePadding, -StagePadding, stageSize + s3d::Size{ 2*StagePadding, 2*StagePadding });
}