#include "GameResult.h"
#include "Score.h"

namespace
{
	std::vector<int> ResultBGMNumbers{
		1, 2, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 17,
	};
}

GameResult::GameResult()
{
}

void GameResult::init()
{
	std::random_shuffle(ResultBGMNumbers.begin(), ResultBGMNumbers.end());
	m_resultBGMName = L"Result" + s3d::ToString(ResultBGMNumbers.at(0));
	s3d::SoundAsset(m_resultBGMName).setLoop(true);
	s3d::SoundAsset(m_resultBGMName).play();
}

void GameResult::update()
{
	const auto& ReplayFont = s3d::FontAsset(L"Large")(L"もう一度プレイする");
	const auto& ReplayFontPos = s3d::Window::Center() + s3d::Vec2{ 0, 200 };
	const auto& ReplayFontRegion = ReplayFont.regionCenter(ReplayFontPos).stretched(10, 10);

	if (ReplayFontRegion.leftClicked) {
		s3d::SoundAsset(L"Decision").playMulti();
		s3d::SoundAsset(m_resultBGMName).stop();
		changeScene(L"GameMain", 2000);
	}

	if (ReplayFontRegion.mouseOver) {
		++m_mouseOverReplay;
	}else {
		m_mouseOverReplay = 0;
	}

	if (m_mouseOverReplay == 1) {
		s3d::SoundAsset(L"Select").playMulti();
		m_stretchableLine.setHiddenRegion(ReplayFontRegion);
		m_stretchableLine.restart();
	}

	const auto& ToTitleFont = s3d::FontAsset(L"Large")(L"タイトルに戻る");
	const auto& ToTitleFontPos = s3d::Window::Center() + s3d::Vec2{ 0, 280 };
	const auto& ToTitleFontRegion = ToTitleFont.regionCenter(ToTitleFontPos).stretched(10, 10);

	if (ToTitleFontRegion.leftClicked) {
		s3d::SoundAsset(L"Decision").playMulti();
		s3d::SoundAsset(m_resultBGMName).stop();
		changeScene(L"GameTitle", 2000);
	}

	if (ToTitleFontRegion.mouseOver) {
		++m_mouseOverToTitle;
	}
	else {
		m_mouseOverToTitle = 0;
	}

	if (m_mouseOverToTitle == 1) {
		s3d::SoundAsset(L"Select").playMulti();
		m_stretchableLine.setHiddenRegion(ToTitleFontRegion);
		m_stretchableLine.restart();
	}

	m_stretchableLine.update();
}

void GameResult::draw() const
{
	s3d::TextureAsset(L"ResultFrame").draw();
	s3d::FontAsset(L"ResultScore")(Score::getInstance()->getScore()).drawCenter(s3d::Window::Center() + s3d::Vec2{0, -20}, s3d::Palette::Black);

	const auto& ReplayFont = s3d::FontAsset(L"Large")(L"もう一度プレイする");
	const auto& ReplayFontPos = s3d::Window::Center() + s3d::Vec2{ 0, 200 };
	const auto& ReplayFontRegion = ReplayFont.regionCenter(ReplayFontPos).stretched(10, 10);

	if (ReplayFontRegion.mouseOver) {
		ReplayFontRegion.drawFrame(1.0, 0.0, s3d::Palette::Black);
		ReplayFont.drawCenter(ReplayFontPos, s3d::Palette::Black);
	}
	else {
		ReplayFont.drawCenter(ReplayFontPos, s3d::Palette::Black);
	}

	const auto& ToTitleFont = s3d::FontAsset(L"Large")(L"タイトルに戻る");
	const auto& ToTitleFontPos = s3d::Window::Center() + s3d::Vec2{ 0, 280 };
	const auto& ToTitleFontRegion = ToTitleFont.regionCenter(ToTitleFontPos).stretched(10, 10);

	if (ToTitleFontRegion.mouseOver) {
		ToTitleFontRegion.drawFrame(1.0, 0.0, s3d::Palette::Black);
		ToTitleFont.drawCenter(ToTitleFontPos, s3d::Palette::Black);
	}
	else {
		ToTitleFont.drawCenter(ToTitleFontPos, s3d::Palette::Black);
	}

	 m_stretchableLine.draw();
}