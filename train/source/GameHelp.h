#pragma once

#include "GameDefine.h"

class GameHelp : public Scene
{
public:
	GameHelp();
	~GameHelp() = default;

	void init() override;
	void update() override;
	void draw() const override;

private:
	int m_current{ 0 };
	std::vector<std::pair<s3d::String, s3d::String>> m_descriptions;
};
