#pragma once

#include "StationParameter.h"
struct Camera;

class StationCircle
{
public:

	enum class State
	{
		InLine,
		InMap,
		InHolder
	};

	StationCircle() = default;
	StationCircle(const StationParameter& parameters, const std::shared_ptr<Camera>& camera);

	~StationCircle() = default;

	void update();
	void update(const std::shared_ptr<Camera>& camera);
	void draw() const;

	void moveRandomPos();
	void moveRandomPos(const std::shared_ptr<Camera>& camera);
	void moveToStationLine();
	void moveToStationLine(const std::shared_ptr<Camera>& camera);
	void moveToStationHolder(const s3d::Vec2& end);

	bool canMoveToStationHolder() const { return m_state == State::InMap && m_circle.intersects(s3d::Mouse::Pos()); }
	bool inHolder() const { return m_state == State::InHolder; }
	bool inLine() const { return m_state == State::InLine; }

	const s3d::Vec2& getPos() const { return m_pos; }
	const StationParameter& getParameter() const { return m_parameter; }

	int getLineNum() const { return m_parameter.inLineParameters.size(); }

	bool inLine(const s3d::String& lineName) const;

	bool operator == (const StationCircle& rhs) const;
	bool operator == (const StationParameter& rhs) const;

private:
	StationParameter m_parameter;

	State m_state{ State::InLine };

	s3d::Vec2 m_pos;
	s3d::Circle m_circle;

	s3d::EasingController<s3d::Vec2> m_easingController;
};