#pragma once

/*!
@brief ある区間の値を保持し続けるカウンター
区間の端までいった場合は、区間の端の値になる
*/
class ReversibleCounter
{
public:
	ReversibleCounter() :
		ReversibleCounter(0)
	{}

	ReversibleCounter(int max) :
		ReversibleCounter(0, max)
	{}

	ReversibleCounter(int val, int max, int min = 0) :
		m_index{ val - min },
		m_min{ min },
		m_max{ max },
		m_size{ (max - min) * 2 },
		m_counterList( m_size )
	{
		for (int i{ 0 }; i < m_size / 2; ++i) {
			m_counterList.at(i) = i + min;
			m_counterList.at(m_size - 1 - i) = i + min + 1;
		}
	}

	ReversibleCounter& operator ++ ()
	{
		m_index = (m_index + 1) % m_counterList.size();
		return (*this);
	}

	ReversibleCounter operator ++ (int)
	{
		ReversibleCounter temp = (*this);
		m_index = (m_index + 1) % m_counterList.size();

		return temp;
	}

	template<typename T>
	ReversibleCounter operator + (T&& rhs) const
	{
		ReversibleCounter temp = (*this);
		temp.m_index = (temp.m_index + rhs) % temp.m_size;

		return temp;
	}

	template<typename T>
	ReversibleCounter operator + (const T& rhs) const
	{
		ReversibleCounter temp = (*this);
		temp.m_index = (temp.m_index + rhs) % temp.m_size();

		return temp;
	}

	template<typename T>
	ReversibleCounter operator - (T&& rhs) const
	{
		ReversibleCounter temp = (*this);
		temp.m_index = (temp.m_size + temp.m_index - rhs) % temp.m_size;

		return temp;
	}

	template<typename T>
	ReversibleCounter operator - (const T& rhs) const
	{
		ReversibleCounter temp = (*this);
		temp.m_index = (temp.m_size + temp.m_index - rhs) % temp.m_size();

		return temp;
	}

	template<typename T>
	operator T() const
	{
		return static_cast<T>(m_counterList.at(m_index));
	}

private:
	int m_index;
	int m_min;
	int m_max;
	int m_size;

	std::vector<int> m_counterList;
};
