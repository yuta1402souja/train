#pragma once

#include "StationParameter.h"

class StationCircle;
class Shifter;

class StationHolder
{
public:
	StationHolder(std::shared_ptr<Shifter> shifter);
	~StationHolder() = default;

	void update();
	void draw() const;

	void setShifter(std::shared_ptr<Shifter> shifter) { m_shifter = shifter; }
	void addStationCircle(std::shared_ptr<StationCircle> circle);

	// StationLineからマウスクリックがあったことを通知する
	void checkMovableToLine(const StationParameter& parameter);

	// 指定した駅が存在するかどうかを返す
	bool contains(const StationParameter& parameter) const;

private:
	s3d::TextureAsset m_frameTexture;

	std::shared_ptr<Shifter> m_shifter;
	std::vector<std::shared_ptr<StationCircle>> m_stationCircles;
};