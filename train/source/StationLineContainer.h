#pragma once

class StationDataBase;
class StationLine;
class Finder;
class Shifter;
struct Camera;

class StationLineContainer
{
public:
	StationLineContainer(const StationDataBase& sdb, std::shared_ptr<Finder> finder, std::shared_ptr<Shifter> shifter, std::shared_ptr<Camera> camera);
	~StationLineContainer();

	void update();
	void draw() const;

private:
	std::shared_ptr<Finder> m_finder;
	std::shared_ptr<Shifter> m_shifter;
	std::shared_ptr<Camera> m_camera;

	std::vector<std::shared_ptr<StationLine>> m_stationLines;
};