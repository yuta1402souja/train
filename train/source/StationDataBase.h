#pragma once

#include "StationParameter.h"

struct StationLineParameter
{
	s3d::String name;
	s3d::String kanaName;
	
	s3d::Vec2 globalPos;

	s3d::Color color;
};

class StationDataBase
{
public:
	using LineName = s3d::String;
	using ParameterList = std::vector<StationParameter>;

	using StationLineMap = std::unordered_map<LineName, StationLineParameter>;
	using StationParameterMap = std::unordered_map<LineName, ParameterList>;

	using iterator = StationLineMap::iterator;
	using const_iterator = StationLineMap::const_iterator;

public:
	StationDataBase();
	StationDataBase(const s3d::String& fileDirectory);
	~StationDataBase() = default;

	iterator begin() { return m_stationLineMap.begin(); }
	const_iterator begin() const { return m_stationLineMap.begin(); }
	iterator end() { return m_stationLineMap.end(); }
	const_iterator end() const { return m_stationLineMap.end(); }

	const s3d::Color& getLineColor(const s3d::String& lineName) const
	{
		return m_stationLineMap.at(lineName).color;
	}

	StationLineParameter getStationLineParameter(const s3d::String& lineName) const
	{
		return m_stationLineMap.at(lineName);
	}

	ParameterList getParameterList(const s3d::String& lineName) const
	{
		return m_stationParameterMap.at(lineName);
	}

	const s3d::RectF& getStageRegion() const { return m_stageRegion; }

private:
	const int StagePadding{ 100 };

	StationLineMap m_stationLineMap;
	StationParameterMap m_stationParameterMap;

	s3d::RectF m_stageRegion;
};