#pragma once

namespace input
{
	enum class Type
	{
		Up,
		Right,
		Down,
		Left
	};

	bool isClicked(Type type);
	bool isReleased(Type type);
	bool isPressed(Type type);
}