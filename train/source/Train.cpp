#include "Train.h"
#include "Camera.h"
#include "StationParameter.h"
#include "Finder.h"
#include "Score.h"
#include "StationDataBase.h"
#include "ReversibleCounter.h"

namespace
{
	const std::unordered_map<s3d::String, s3d::Rect> LineNameToTrainRegion{
		{ L"NgH", {   0,   0, 64, 64 } },
		{ L"NgM", {  64,  64, 64, 64 } },
		{ L"NgT", { 128, 128, 64, 64 } },
		{ L"NgS", { 192, 192, 64, 64 } },
		{ L"NgK", { 256, 256, 64, 64 } },

		{ L"TmG" , {   0, 0, 64, 64 } },
		{ L"TmM" , {  64, 0, 64, 64 } },
		{ L"TmMh", { 128, 0, 64, 64 } },
		{ L"TmH" , { 192, 0, 64, 64 } },
		{ L"TmT" , { 256, 0, 64, 64 } },
		{ L"TmC" , { 320, 0, 64, 64 } },
		{ L"TmY" , { 384, 0, 64, 64 } },
		{ L"TmZ" , { 448, 0, 64, 64 } },
		{ L"TmN" , { 512, 0, 64, 64 } },
		{ L"TmF" , { 576, 0, 64, 64 } },
		{ L"TtA" , { 640, 0, 64, 64 } },
		{ L"TtI" , { 704, 0, 64, 64 } },
		{ L"TtS" , { 768, 0, 64, 64 } },
		{ L"TtE" , { 832, 0, 64, 64 } },

		{ L"OsM" , {   0, 0, 64, 64 } },
		{ L"OsT" , {  64, 0, 64, 64 } },
		{ L"OsY" , { 128, 0, 64, 64 } },
		{ L"OsC" , { 192, 0, 64, 64 } },
		{ L"OsS" , { 256, 0, 64, 64 } },
		{ L"OsK" , { 320, 0, 64, 64 } },
		{ L"OsN" , { 384, 0, 64, 64 } },
		{ L"OsI" , { 448, 0, 64, 64 } },
	};

	const std::unordered_map<s3d::String, s3d::String> LineToTrainsFileName{
		{ L"NgH", L"NagoyaTrains" },
		{ L"NgM", L"NagoyaTrains" },
		{ L"NgT", L"NagoyaTrains" },
		{ L"NgS", L"NagoyaTrains" },
		{ L"NgK", L"NagoyaTrains" },

		{ L"TmG" , L"TokyoTrains" },
		{ L"TmM" , L"TokyoTrains" },
		{ L"TmMh", L"TokyoTrains" },
		{ L"TmH" , L"TokyoTrains" },
		{ L"TmT" , L"TokyoTrains" },
		{ L"TmC" , L"TokyoTrains" },
		{ L"TmY" , L"TokyoTrains" },
		{ L"TmZ" , L"TokyoTrains" },
		{ L"TmN" , L"TokyoTrains" },
		{ L"TmF" , L"TokyoTrains" },
		{ L"TtA" , L"TokyoTrains" },
		{ L"TtI" , L"TokyoTrains" },
		{ L"TtS" , L"TokyoTrains" },
		{ L"TtE" , L"TokyoTrains" },

		{ L"OsM" , L"OsakaTrains" },
		{ L"OsT" , L"OsakaTrains" },
		{ L"OsY" , L"OsakaTrains" },
		{ L"OsC" , L"OsakaTrains" },
		{ L"OsS" , L"OsakaTrains" },
		{ L"OsK" , L"OsakaTrains" },
		{ L"OsN" , L"OsakaTrains" },
		{ L"OsI" , L"OsakaTrains" },
	};

	const std::unordered_map<s3d::String, s3d::String> LineToSideTrainsFileName{
		{ L"NgH", L"NagoyaSideTrains" },
		{ L"NgM", L"NagoyaSideTrains" },
		{ L"NgT", L"NagoyaSideTrains" },
		{ L"NgS", L"NagoyaSideTrains" },
		{ L"NgK", L"NagoyaSideTrains" },

		{ L"TmG" , L"TokyoSideTrains" },
		{ L"TmM" , L"TokyoSideTrains" },
		{ L"TmMh", L"TokyoSideTrains" },
		{ L"TmH" , L"TokyoSideTrains" },
		{ L"TmT" , L"TokyoSideTrains" },
		{ L"TmC" , L"TokyoSideTrains" },
		{ L"TmY" , L"TokyoSideTrains" },
		{ L"TmZ" , L"TokyoSideTrains" },
		{ L"TmN" , L"TokyoSideTrains" },
		{ L"TmF" , L"TokyoSideTrains" },
		{ L"TtA" , L"TokyoSideTrains" },
		{ L"TtI" , L"TokyoSideTrains" },
		{ L"TtS" , L"TokyoSideTrains" },
		{ L"TtE" , L"TokyoSideTrains" },

		{ L"OsM" , L"OsakaSideTrains" },
		{ L"OsT" , L"OsakaSideTrains" },
		{ L"OsY" , L"OsakaSideTrains" },
		{ L"OsC" , L"OsakaSideTrains" },
		{ L"OsS" , L"OsakaSideTrains" },
		{ L"OsK" , L"OsakaSideTrains" },
		{ L"OsN" , L"OsakaSideTrains" },
		{ L"OsI" , L"OsakaSideTrains" },
	};

	const std::unordered_map<s3d::String, s3d::Rect> SideTrainRegionMap{
		{ L"NgH" , {   0, 0, 64, 24 } },
		{ L"NgM" , {  64, 0, 64, 24 } },
		{ L"NgT" , { 128, 0, 64, 24 } },
		{ L"NgS" , { 192, 0, 64, 24 } },
		{ L"NgK" , { 256, 0, 64, 24 } },

		{ L"TmG" , {   0, 0, 64, 24 } },
		{ L"TmM" , {  64, 0, 64, 24 } },
		{ L"TmMh", { 128, 0, 64, 24 } },
		{ L"TmH" , { 192, 0, 64, 24 } },
		{ L"TmT" , { 256, 0, 64, 24 } },
		{ L"TmC" , { 320, 0, 64, 24 } },
		{ L"TmY" , { 384, 0, 64, 24 } },
		{ L"TmZ" , { 448, 0, 64, 24 } },
		{ L"TmN" , { 512, 0, 64, 24 } },
		{ L"TmF" , { 576, 0, 64, 24 } },
		{ L"TtA" , { 640, 0, 64, 24 } },
		{ L"TtI" , { 704, 0, 64, 24 } },
		{ L"TtS" , { 768, 0, 64, 24 } },
		{ L"TtE" , { 832, 0, 64, 24 } },

		{ L"OsM" , {   0, 0, 64, 24 } },
		{ L"OsT" , {  64, 0, 64, 24 } },
		{ L"OsY" , { 128, 0, 64, 24 } },
		{ L"OsC" , { 192, 0, 64, 24 } },
		{ L"OsS" , { 256, 0, 64, 24 } },
		{ L"OsK" , { 320, 0, 64, 24 } },
		{ L"OsN" , { 384, 0, 64, 24 } },
		{ L"OsI" , { 448, 0, 64, 24 } },
	};
}

Train::Train(const StationDataBase& sdb, const s3d::String & lineName, std::shared_ptr<Finder> finder) :
	m_finder{ finder },
	m_lineName{ lineName },
	m_lineColor{ sdb.getLineColor(lineName) },
	m_currentLink{ sdb.getParameterList(lineName).size() - 1 }
{
	const auto& list = sdb.getParameterList(lineName);

	for (const auto& param : list) {
		m_stationLinks.emplace_back(param);
	}

	const s3d::Point& pos1{ m_stationLinks.at(m_currentLink).pos.asPoint() };
	const s3d::Point& pos2{ m_stationLinks.at(m_currentLink + 1).pos.asPoint() };

	const double distance = pos1.distanceFrom(pos2);
	m_nextTime = distance / Speed * 60;
}

void Train::update(std::shared_ptr<Camera> camera)
{
	// とりあえず1秒で1駅進む
	if (m_timer >= m_nextTime) {
		++m_currentLink;

		if (!m_finder->findInLine(m_stationLinks.at(m_currentLink))) {
			Score::getInstance()->addScore(-100);
		}

		const auto& pos1{ m_stationLinks.at(m_currentLink).pos.asPoint() };
		const auto& pos2{ m_stationLinks.at(m_currentLink + 1).pos.asPoint() };

		const double distance = pos1.distanceFrom(pos2);
		m_nextTime = distance / Speed * 60;

		m_timer = 0;
	}

	m_nextEmptyCircleCount = 0;

	for (int i = 1; i < 4; ++i) {
		if (!m_finder->findInLine(m_stationLinks.at(m_currentLink + i))) {
			m_nextEmptyCircleCount = i;
			break;
		}
	}

	const auto& pos1{ m_stationLinks.at(m_currentLink).pos.asPoint() };
	const auto& pos2{ m_stationLinks.at(m_currentLink + 1).pos.asPoint() };

	const double ratio = static_cast<double>(m_timer) / m_nextTime;
	m_pos = s3d::Vec2{ pos1 * (1 - ratio) + pos2 * ratio } - camera->pos;

	// 追従モードはこれをコメントアウト
	// m_stationLine->>m_camera->pos = m_pos - s3d::Window::Size() / 2;

	++m_timer;
}

void Train::draw() const
{
	const auto& pos1{ m_stationLinks.at(m_currentLink).pos.asPoint() };
	const auto& pos2{ m_stationLinks.at(m_currentLink + 1).pos.asPoint() };

	const auto& d = pos2 - pos1;

	const auto& rotate_rad{ s3d::Math::Acos((d.x) / d.distanceFrom({0, 0})) };

	const double theta{ s3d::Math::Abs(s3d::Math::Sin(s3d::Math::Pi * m_timer / 60)) };
	const s3d::uint32 alpha{ static_cast<s3d::uint32>(255 * theta) };

	if (d.x >= 0) {
		if (d.y > 0) {
			s3d::TextureAsset(LineToSideTrainsFileName.at(m_lineName))(SideTrainRegionMap.at(m_lineName)).mirror().rotate(rotate_rad).drawAt({ m_pos.asPoint() });
		}
		else {
			s3d::TextureAsset(LineToSideTrainsFileName.at(m_lineName))(SideTrainRegionMap.at(m_lineName)).mirror().rotate(-rotate_rad).drawAt({ m_pos.asPoint() });
		}
	}
	else {
		if (d.y > 0) {
			s3d::TextureAsset(LineToSideTrainsFileName.at(m_lineName))(SideTrainRegionMap.at(m_lineName)).flip().mirror().rotate(rotate_rad).drawAt({ m_pos.asPoint() });
		}
		else {
			s3d::TextureAsset(LineToSideTrainsFileName.at(m_lineName))(SideTrainRegionMap.at(m_lineName)).flip().mirror().rotate(-rotate_rad).drawAt({ m_pos.asPoint() });
		}
	}

	s3d::Circle(m_pos, 10).draw(s3d::Color{m_lineColor, alpha});

	if (m_nextEmptyCircleCount) {
		const s3d::Vec2& dirVec = s3d::Window::Center() - m_pos;
		const s3d::Vec2& attentionCirclePos = m_pos + dirVec.normalized() * 100;

		const int x{ s3d::Clamp(static_cast<int>(attentionCirclePos.x), 100, s3d::Window::Width() - 200) };
		const int y{ s3d::Clamp(static_cast<int>(attentionCirclePos.y), 100, s3d::Window::Height() - 200) };

		double distance = 0;

		for (int i = 1; i < m_nextEmptyCircleCount; ++i) {
			const auto& pos1{ m_stationLinks.at(m_currentLink + i).pos.asPoint() };
			const auto& pos2{ m_stationLinks.at(m_currentLink + i + 1).pos.asPoint() };

			distance += pos1.distanceFrom(pos2);
		}

		int nextEmptyCircleTime = distance / Speed * 60;
		nextEmptyCircleTime += m_nextTime - m_timer;

		s3d::Line{ m_pos, { x, y } }.draw(2.0, m_lineColor);
		s3d::Circle{ { x, y }, 60 }.draw(s3d::Palette::White);
		s3d::Circle{ { x, y }, 60 }.drawFrame(5.0, 0, m_lineColor);

		s3d::TextureAsset(LineToTrainsFileName.at(m_lineName))(LineNameToTrainRegion.at(m_lineName)).drawAt({ x, y - 10 });

		const int afterPoint = static_cast<int>((nextEmptyCircleTime / 60.0 * 10)) % 10;
		const s3d::String text = s3d::Format(nextEmptyCircleTime / 60, L'.', afterPoint, L"s");
		s3d::FontAsset(L"Small")(text).drawCenter({ x, y + 40}, s3d::Palette::Black);
	}
}