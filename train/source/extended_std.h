#pragma once

#include <algorithm>

/*!
@brief Extended std
@note c.begin(), c.end()はstd::begin(), std::end()を使った方がいいかも
*/
namespace estd
{
	template<typename Container>
	void sort(Container& c)
	{
		std::sort(c.begin(), c.end());
	}

	template<typename Container, typename Pred>
	void sort(Container& c, Pred p)
	{
		std::sort(c.begin(), c.end(), p);
	}

	template<typename Container, typename Value>
	Container& remove_erase(Container& c, const Value& v)
	{
		c.erase(std::remove(c.begin(), c.end(), v), c.end());
		return c;
	}

	template<typename Container, typename Pred>
	Container& remove_erase_if(Container& c, Pred p)
	{
		c.erase(std::remove_if(c.begin(), c.end(), p), c.end());
		return c;
	}
}