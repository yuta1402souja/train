#pragma once

#include "GameDefine.h"
#include "StretchableLine.h"

class GameResult : public Scene
{
public:
	GameResult();
	~GameResult() = default;

	void init() override;
	void update() override;
	void draw() const override;

private:
	int m_mouseOverReplay{ 0 };
	int m_mouseOverToTitle{ 0 };

	s3d::String m_resultBGMName;

	StretchableLine m_stretchableLine;
};
