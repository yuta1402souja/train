#include "GameMain.h"
#include "StationDataBase.h"
#include "Camera.h"
#include "StationCircle.h"
#include "StationHolder.h"
#include "StationCircleContainer.h"
#include "StationLineContainer.h"
#include "Shifter.h"
#include "Finder.h"
#include "TrainContainer.h"
#include "GameTimer.h"
#include "Score.h"
#include "StageStore.h"

GameMain::GameMain()
{
}

void GameMain::init()
{
	StationDataBase sdb{StageStore::getInstance()->getStage(m_data->currentStageName)};

	const s3d::Vec2 cameraStartPos{ sdb.getStageRegion().center - s3d::Window::Size() / 2 };

	m_camera = std::make_shared<Camera>(cameraStartPos, sdb.getStageRegion() );
	m_cameraWork = std::make_unique<CameraWork>(m_camera);

	m_shifter = std::make_shared<Shifter>();
	m_stationHolder = std::make_shared<StationHolder>(m_shifter);
	m_stationCircleContainer = std::make_shared<StationCircleContainer>(m_shifter, m_camera);
	m_finder = std::make_shared<Finder>(m_stationCircleContainer);
	m_stationLineContainer = std::make_shared<StationLineContainer>(sdb, m_finder, m_shifter, m_camera);
	m_trainContainer = std::make_shared<TrainContainer>(sdb, m_finder, m_camera);

	// DataBaseをもとにStationCircleContainerにSCを追加していく
	for (const auto& line : sdb) {
		const auto& lineName = line.first;

		for (const auto& c : sdb.getParameterList(lineName)) {
			m_stationCircleContainer->addStationCircle(std::make_shared<StationCircle>(c, m_camera));
		}
	}

	m_shifter->setStationHolder(m_stationHolder);
	m_shifter->setStationCircleContainer(m_stationCircleContainer);

	m_gameTimer = std::make_unique<GameTimer>();
	m_gameTimer->start();

	s3d::SoundAsset(L"GameMain").setLoop(true);
	s3d::SoundAsset(L"GameMain").play();
}

void GameMain::update()
{
	if (m_gameTimer->isEnd()) {
		s3d::SoundAsset(L"GameMain").stop();
		s3d::SoundAsset(L"Finish").playMulti();
		changeScene(L"GameResult", 2000);
	}

	m_cameraWork->update();
	m_stationLineContainer->update();
	m_stationCircleContainer->update();
	m_trainContainer->update();
	m_stationHolder->update();
	m_gameTimer->update();

	Score::getInstance()->update();
}

void GameMain::updateFadeIn(double t)
{
	GameMain::update();
}

void GameMain::draw() const
{
	m_stationLineContainer->draw();
	m_stationCircleContainer->draw();
	m_trainContainer->draw();
	m_stationHolder->draw();
	m_cameraWork->draw();
	m_gameTimer->draw();

	Score::getInstance()->draw();
}