#include "StretchableLine.h"

StretchableLine::StretchableLine() :
	StretchableLine({ 0, 0, 0, 0 })
{
}

StretchableLine::StretchableLine(const s3d::Rect& hiddenRegion) :
	m_pos{ hiddenRegion.center },
	m_leftStartPos{ m_pos.movedBy(-hiddenRegion.w / 2, 0) },
	m_rightStartPos{ m_pos.movedBy(hiddenRegion.w / 2, 0) },
	m_leftLine{ m_leftStartPos, m_leftStartPos },
	m_rightLine{ m_rightStartPos, m_rightStartPos }
{
}

void StretchableLine::restart()
{
	m_stopwatch.restart();
}

void StretchableLine::update()
{
	if (m_stopwatch.ms() > m_stretchingTime) {
		m_stopwatch.pause();
		m_stopwatch.set(s3d::MillisecondsF{ m_stretchingTime });
	}

	const auto& ms = m_stopwatch.ms();
	const double ratio = static_cast<double>(ms) / m_stretchingTime;

	const s3d::Vec2 leftEndPos{ 0, m_pos.y };
	const s3d::Vec2 rightEndPos{ s3d::Window::Width(), m_pos.y};

	m_leftLine.set(m_leftStartPos, m_leftStartPos * (1 - ratio) + leftEndPos * ratio);
	m_rightLine.set(m_rightStartPos, m_rightStartPos * (1 - ratio) + rightEndPos * ratio);
}

void StretchableLine::draw() const
{
	m_leftLine.draw(s3d::Palette::Black);
	m_rightLine.draw(s3d::Palette::Black);
}

void StretchableLine::setHiddenRegion(const s3d::Rect& hiddenRegion)
{
	setHiddenRegion(hiddenRegion);
}

void StretchableLine::setHiddenRegion(const s3d::RectF& hiddenRegion)
{
	m_pos.set(hiddenRegion.center);

	m_leftStartPos.set(m_pos.movedBy(-hiddenRegion.w / 2, 0));
	m_rightStartPos.set(m_pos.movedBy(hiddenRegion.w / 2, 0));

	m_leftLine.set(m_leftStartPos, m_leftStartPos);
	m_rightLine.set(m_rightStartPos, m_rightStartPos);
}