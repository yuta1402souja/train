#pragma once

#include "GameDefine.h"
#include "StretchableLine.h"


class StageSelection : public Scene
{
public:
	StageSelection();
	~StageSelection() = default;

	void init() override;
	void update() override;
	void updateFadeIn(double t) override;
	void draw() const override;

private:
	const s3d::Vec2 DecisionFontPos{ s3d::Window::Center().movedBy(0, 280) };

	const s3d::Circle m_leftButton{ s3d::Window::Center().movedBy(-470, 0), 30 };
	const s3d::Circle m_rightButton{ s3d::Window::Center().movedBy(470, 0), 30 };

	const s3d::Circle m_helpButton{ { 960, 720 }, 30 };

	StageName m_currentStageName{ StageName::Nagoya };

	StretchableLine m_strechableLine;
	StretchableLine m_decisionStrechableLine;

	int m_mouseOverDecision{ 0 };
};