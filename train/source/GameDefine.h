#pragma once

enum class StageName
{
	Nagoya,
	Tokyo,
	Osaka,
};

const static int StageNum = 3;

struct GameData
{
	StageName currentStageName{ StageName::Nagoya };
};

using Scene = ham::SceneManager<s3d::String, GameData>::Scene;