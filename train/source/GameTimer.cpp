#include "GameTimer.h"

GameTimer::GameTimer() :
	m_size(320, 80),
	m_pos((s3d::Window::Width() - m_size.x) / 2, 10),
	m_backRect(m_pos, m_size, 20)
{
}

void GameTimer::start()
{
	m_timer.start();
}
void GameTimer::restart()
{
	m_timer.restart();
}

void GameTimer::pause()
{
	m_timer.pause();
}
void GameTimer::resume()
{
	m_timer.resume();
}

void GameTimer::update()
{
	m_timeLeft = TimeLimit - std::min(TimeLimit, m_timer.ms());
}

void GameTimer::draw() const
{
	// m_backRect.draw(s3d::Color(0, 0, 0, 150));

	const std::chrono::milliseconds msec(m_timeLeft);
	const std::chrono::minutes min = std::chrono::duration_cast<std::chrono::minutes>(msec);
	const std::chrono::seconds sec = std::chrono::duration_cast<std::chrono::seconds>(msec) - min;

	/*
	等幅フォントの場合これで解決する
	const s3d::String text = s3d::Format(s3d::Pad(min.count(), { 2, L'0' }), L':', s3d::Pad(sec.count(), { 2, L'0' }));
	s3d::FontAsset(L"Timer")(text).drawCenter(0);
	*/

	const s3d::Vec2 c_pos = m_pos + m_size / 2;

	s3d::FontAsset(L"Timer")(L':').drawCenter(c_pos);
	s3d::FontAsset(L"Timer")(s3d::Pad(min.count(), { 2, L'0' })).drawCenter(c_pos - s3d::Vec2(65, 0));
	s3d::FontAsset(L"Timer")(s3d::Pad(sec.count(), { 2, L'0' })).drawCenter(c_pos + s3d::Vec2(65, 0));
}