#pragma once

struct Camera
{
	s3d::Vec2 pos;
	s3d::RectF region;

	Camera() = default;
	Camera(const s3d::Vec2& _pos) : pos(_pos) {}
	Camera(const s3d::Vec2& _pos, const s3d::RectF& _region) : pos(_pos), region(_region) {}
};

class CameraWork
{
public:
	CameraWork();
	CameraWork(std::shared_ptr<Camera> camera);
	CameraWork(const s3d::Vec2& pos);

	void update();
	void draw() const;

	void setCamera(std::shared_ptr<Camera> camera) { m_camera = camera; }

private:
	std::shared_ptr<Camera> m_camera;

	const int Width{ 80 };

	const s3d::Rect TopRect{ {0, 0}, {s3d::Window::Width(), Width} };
	const s3d::Rect RightRect{ {s3d::Window::Width() - Width, 0}, {Width, s3d::Window::Height()} };
	const s3d::Rect BottomRect{ {0, s3d::Window::Height() - Width}, {s3d::Window::Width(), Width} };
	const s3d::Rect LeftRect{ {0, 0}, {Width, s3d::Window::Height()} };
};