#pragma once

#include "GameDefine.h"
#include "StretchableLine.h"

class GameTitle : public Scene
{
public:
	GameTitle();
	~GameTitle() = default;

	void init() override;
	void update() override;
	void draw() const override;

private:
	int m_mouseOverStart{ 0 };
	int m_mouseOverEnd{ 0 };

	const s3d::Vec2 m_startFontPos{ 800, 500 };
	const s3d::Vec2 m_endFontPos{ 800, 560 };

	StretchableLine m_stretchableLine;
};
