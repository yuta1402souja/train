#pragma once

#define NO_S3D_USING
#define NO_MATH_USING
#include <Siv3D.hpp>

#define NO_HAM_USING
#include <HamFramework.hpp>