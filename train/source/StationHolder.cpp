#include "StationHolder.h"
#include "StationCircle.h"
#include "StationParameter.h"
#include "Shifter.h"
#include "input.h"

namespace
{
	const s3d::Size HolderWindowSize{ 0, 580 };

	void adjustCirclePos(std::vector<std::shared_ptr<StationCircle>>& stationCircles)
	{
		const int size{ static_cast<int>(stationCircles.size()) };
		const int colSize{ size / 4 };

		for (int i = 0; i < stationCircles.size(); ++i) {
			const int row{ i  % 4 };
			auto& c{ stationCircles.at(i) };
			c->moveToStationHolder({ HolderWindowSize.x + 30 + i * 20, HolderWindowSize.y + 30 + row * 40});
		}
	}
}

StationHolder::StationHolder(std::shared_ptr<Shifter> shifter) :
	m_frameTexture{ s3d::TextureAsset(L"IndicatorFrame") },
	m_shifter{ shifter }
{
}

void StationHolder::update()
{
	for (auto&& c : m_stationCircles) {
		c->update();
	}
}

void StationHolder::draw() const
{
	m_frameTexture.draw();

	// 逆順に描画
	for (int i = m_stationCircles.size() - 1; i >= 0; --i) {
		const auto& c = m_stationCircles.at(i);
		c->draw();
	}
}

void StationHolder::addStationCircle(std::shared_ptr<StationCircle> stationCircle)
{
	auto it = std::upper_bound(
		m_stationCircles.begin(),
		m_stationCircles.end(),
		stationCircle,
		[](const std::shared_ptr<StationCircle>& c1, const std::shared_ptr<StationCircle>& c2) {
			return c1->getLineNum() < c2->getLineNum();
		}
	);

	m_stationCircles.insert(it, stationCircle);
	adjustCirclePos(m_stationCircles);
}

void StationHolder::checkMovableToLine(const StationParameter & parameter)
{
	auto it = std::find_if(m_stationCircles.begin(), m_stationCircles.end(), [parameter](const std::shared_ptr<StationCircle>& c) {
		return c->getParameter() == parameter;
	});

	if (it == m_stationCircles.end()) { return; }

	s3d::SoundAsset(L"PutStation").playMulti();

	m_shifter->shiftToStationCircleContainer(*it);
	m_stationCircles.erase(it);

	// 今ある電車の位置修正
	adjustCirclePos(m_stationCircles);
}

bool StationHolder::contains(const StationParameter& parameter) const
{
	auto it = std::find_if(
		m_stationCircles.begin(),
		m_stationCircles.end(),
		[parameter](const std::shared_ptr<StationCircle>& c) { return c->getParameter() == parameter; }
	);

	if (it == m_stationCircles.end()) { return false; }

	return true;
}
