#include "StationCircle.h"
#include "Camera.h"

namespace
{
	const double Rsqrt2{ s3d::Math::Rsqrt(2) };
	const std::array<s3d::Vec2, 8> EightDirection {
		{
			{ 0, -1 }, { Rsqrt2 , -Rsqrt2 }, { 1, 0 }, { Rsqrt2, Rsqrt2 },
			{ 0, 1 }, { -Rsqrt2, Rsqrt2 }, { -1, 0 }, { -Rsqrt2, -Rsqrt2 }
		}
	};

	void easingMove(s3d::EasingController<s3d::Vec2>& ec, const s3d::Vec2& pos, const s3d::Vec2& end)
	{
		ec = s3d::EasingController<s3d::Vec2>(pos, end, s3d::Easing::Cubic);
		ec.start();
	}
}

StationCircle::StationCircle(const StationParameter &parameter, const std::shared_ptr<Camera>& camera) :
	m_parameter{ parameter },
	m_pos{ m_parameter.pos },
	m_circle{ m_pos, 10 * (m_parameter.inLineParameters.size() + 1) },
	m_easingController{ m_pos, m_pos }
{
}

void StationCircle::update()
{
	m_pos = m_easingController.easeInOut();
	m_circle.setPos(m_pos);
}

void StationCircle::update(const std::shared_ptr<Camera>& camera)
{
	m_pos = m_easingController.easeInOut() - camera->pos;
	m_circle.setPos(m_pos);
}

void StationCircle::draw() const
{
	switch (m_state)
	{
	case StationCircle::State::InLine:
		m_circle.drawShadow({ 4, 4 }, 10);
		break;
	case StationCircle::State::InMap:
		m_circle.drawShadow({ 12, 12 }, 10);
		break;
	case StationCircle::State::InHolder:
		m_circle.drawShadow({ 12, 12 }, 10);
		break;
	default:
		break;
	}

	m_circle.draw(s3d::Palette::White);

	const int lineNum = m_parameter.inLineParameters.size();

	const auto& nameFont = s3d::FontAsset(L"Small")(m_parameter.name);
	const auto& nameFontRegion = nameFont.region().size;
	const auto& unitVec = EightDirection.at(m_parameter.drawNameDirection);
	const auto& namePos = m_pos + 10 * (lineNum + 1) * unitVec + 20 * unitVec + s3d::Vec2{ unitVec.x * nameFontRegion.x / 2, 0 };

	switch (m_state)
	{
	case StationCircle::State::InLine:
		nameFont.drawCenter(namePos, s3d::Palette::Black);
		break;
	case StationCircle::State::InMap:
		nameFont.drawCenter(namePos, s3d::Palette::Black);
		break;
	case StationCircle::State::InHolder:
		break;
	default:
		break;
	}

	for (int i{ 0 }; i < m_parameter.inLineParameters.size(); ++i) {
		const auto& inLineParam{ m_parameter.inLineParameters.at(i) };
		s3d::FontAsset(L"Small").drawCenter(inLineParam.id, m_pos - s3d::Vec2{ 0, -m_circle.r + (i + 1) * 2.0 * m_circle.r / (lineNum + 1) }, s3d::Palette::Black);
		s3d::Circle{ m_pos, m_circle.r + i * 2 }.drawFrame(0, 2.0, inLineParam.color);
	}
}

void StationCircle::moveRandomPos()
{
	easingMove(m_easingController, m_pos, s3d::RandomVec2(s3d::Window::ClientRect()));
	m_state = State::InMap;
}

void StationCircle::moveRandomPos(const std::shared_ptr<Camera>& camera)
{
	easingMove(m_easingController, m_pos + camera->pos, s3d::RandomVec2(camera->region));
	m_state = State::InMap;
}

void StationCircle::moveToStationHolder(const s3d::Vec2& end)
{
	easingMove(m_easingController, m_pos, end);
	m_state = State::InHolder;
}

void StationCircle::moveToStationLine()
{
	easingMove(m_easingController, m_pos, m_parameter.pos);
	m_state = State::InLine;
}

void StationCircle::moveToStationLine(const std::shared_ptr<Camera>& camera)
{
	easingMove(m_easingController, m_pos + camera->pos, m_parameter.pos);
	m_state = State::InLine;
}

bool StationCircle::inLine(const s3d::String& lineName) const
{
	const auto& inLineParams = m_parameter.inLineParameters;
	auto it = std::find_if(
		inLineParams.begin(),
		inLineParams.end(),
		[lineName](const InLineParameter& param) { return param.lineName == lineName; }
	);

	if (it == inLineParams.end()) {
		return false;
	}

	return true;
}

bool StationCircle::operator == (const StationCircle& rhs) const
{
	return m_parameter == rhs.getParameter();
}

bool StationCircle::operator == (const StationParameter& rhs) const
{
	return m_parameter == rhs;
}