#pragma once

/*!
@brief ある路線での駅のパラメータ(id、色、路線のグローバル座標、路線でのローカル座標)
*/
struct InLineParameter
{
	s3d::String lineName;
	s3d::String id;
	s3d::Color color;
	s3d::Vec2 linePos;
	s3d::Vec2 localPos;

	InLineParameter() = default;
	InLineParameter(const s3d::String& _lineName, const s3d::String _id, const s3d::Color& _color, const s3d::Vec2& _linePos, const s3d::Vec2& _localPos) :
		lineName{ _lineName },
		id{ _id },
		color{ _color },
		linePos{ _linePos },
		localPos{ _localPos }
	{}

	bool operator < (const InLineParameter& rhs) const {
		return id < rhs.id;
	}

	bool operator == (const InLineParameter& rhs) const {
		return id == rhs.id;
	}
};

/*!
@brief 駅のパラメータ(名前、かな、座標、路線ごとの情報)
*/
struct StationParameter
{
	s3d::String name;
	s3d::String kanaName;

	s3d::Vec2 pos;

	int drawNameDirection;

	std::vector<InLineParameter> inLineParameters;

	StationParameter() = default;
	StationParameter(const s3d::String& _name, const s3d::String& _kanaName, const s3d::Vec2& _pos, int _drawNameDirection);

	void addInLineParameter(const InLineParameter& inLineParameter);

	bool operator == (const StationParameter& rhs) const;
};