#pragma once

#include "StationParameter.h"

class StationLine;
class StationHolder;
class StationCircle;
class StationCircleContainer;

/*!
@param StationCircleをクラス間でやりとりする
@note setStationHolder, setStationLineで設定してからでないと使えないので注意
*/
class Shifter
{
public:
	Shifter();
	~Shifter() = default;

	void setStationHolder(std::shared_ptr<StationHolder> stationHolder) { m_stationHolder = stationHolder; }
	void setStationCircleContainer(std::shared_ptr<StationCircleContainer> stationCircleContainer) { m_stationCircleContainer = stationCircleContainer; }

	void shiftToStationHolder(std::shared_ptr<StationCircle> stationCircle);
	void shiftToStationCircleContainer(std::shared_ptr<StationCircle> stationCircle);

	void checkMovableToContainer(const StationParameter& param);
	bool isContainsInHolder(const StationParameter& param) const;

private:

	std::shared_ptr<StationHolder> m_stationHolder;
	std::shared_ptr<StationCircleContainer> m_stationCircleContainer;
};