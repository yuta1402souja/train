#pragma once

class StationCircleContainer;
struct StationParameter;

class Finder
{
public:
	Finder(const std::shared_ptr<StationCircleContainer>& stationCircleContainer);
	~Finder();

	bool findInLine(const StationParameter& stationParameter) const;

private:
	const std::shared_ptr<StationCircleContainer> m_stationCircleContainer;
};