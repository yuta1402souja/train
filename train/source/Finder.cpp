#include "Finder.h"
#include "StationCircleContainer.h"
#include "StationParameter.h"

Finder::Finder(const std::shared_ptr<StationCircleContainer>& stationCircleContainer) :
	m_stationCircleContainer(stationCircleContainer)
{
}

Finder::~Finder()
{
}

bool Finder::findInLine(const StationParameter& stationParameter) const
{
	return m_stationCircleContainer->findInLine(stationParameter);
}